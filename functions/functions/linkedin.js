const functions = require('firebase-functions');
const crypto = require('crypto');
const request = require('request');
const admin = require('firebase-admin');
const db = admin.firestore();
const auth = admin.auth();
const FieldValue = require('firebase-admin').firestore.FieldValue;

// LinkedIn authentication configs
const OAUTH_SCOPES = ['r_liteprofile', 'r_emailaddress', 'w_member_social'];
const environment = functions.config();
const oauth_url = 'https://www.linkedin.com/oauth/v2';
const function_url = environment.service.function_url;
const redirect_localhost = 'http://localhost:8080';
const redirect_production = `${environment.service.limitless_url}`;
const api_url = 'https://api.linkedin.com/v2';

/**
 * Redirects the user to the LinkedIn authentication consent screen.
 * Also the 'state' cookie is set for later state verification.
 */
exports.redirect = functions.https.onRequest((req, res) => {
  const state = crypto.randomBytes(20).toString('hex');
  try {
    const redirectUrl = `${oauth_url}/authorization?response_type=code&client_id=${
      environment.linkedin.client_id
    }&redirect_uri=${function_url}redirectToToken&state=${state}&scope=${OAUTH_SCOPES.join(',')}`;

    return res.redirect(redirectUrl);
  } catch (error) {
    functions.logger.error('Redirect Error: ', error);
    return res.jsonp({ error: error.toString });
  }
});

/**
 * Redirectto token function for creating Firebase access tokens for the current user
 */
exports.redirectToToken = functions.https.onRequest((req, res) => {
  const { code, state } = req.query;
  // functions.logger.info("code: ", code);
  // functions.logger.info("state: ", state);

  if (code && state) {
    const redirectUrl = `${function_url}token?code=${encodeURIComponent(code)}&state=${encodeURIComponent(
      state,
    )}&callback=${function_url}redirectToToken"`;
    return res.redirect(redirectUrl);
  } else return res.jsonp({ error: 'Invalid token or state...' });
});

/**
 * Exchanges a given LinkedIn auth code passed in the 'code' URL query parameter for a Firebase auth token.
 * The request also needs to specify a 'state' query parameter which will be checked against the 'state' cookie.
 * The Firebase custom auth token is sent back in a JSONP callback function with function name defined by the
 * 'callback' query parameter.
 */
exports.token = functions.https.onRequest(async (req, res) => {
  const url = `${oauth_url}/accessToken?grant_type=authorization_code&code=${
    req.query.code
  }&redirect_uri=${function_url}redirectToToken&client_id=${environment.linkedin.client_id}&client_secret=${
    environment.linkedin.client_secret
  }`;

  try {
    const response = await invoke('POST', url, {
      'content-type': 'application/x-www-form-urlencoded',
    });

    const linkedInAccessToken = response.access_token;
    functions.logger.info('LinkedIn Access Token: ', linkedInAccessToken);

    const profileObject = await getProfileObject(linkedInAccessToken);
    // functions.logger.info("profileObject: ", profileObject);
    const userUID = profileObject.id;
    const fullname = getFullname(profileObject);
    // functions.logger.info("userUID: ", userUID);
    // functions.logger.info("fullname: ", fullname);

    const vanityNameObject = await getVanityNameObject(linkedInAccessToken, userUID);
    functions.logger.info('vanityNameObject: ', vanityNameObject);
    const vanityName = getVanityName(vanityNameObject);
    functions.logger.info('vanityName: ', vanityName);

    const emailObject = await getEmailObject(linkedInAccessToken);
    // functions.logger.info("emailObject: ", emailObject);
    const emailAddress = getEmailAddress(emailObject);
    functions.logger.info('emailAddress: ', emailAddress);

    const photoObject = await getPhotoObject(linkedInAccessToken);
    // functions.logger.info("photoObject: ", photoObject);
    const photoUrl = await extractPhotoUrl(photoObject);
    functions.logger.info('photoUrl: ', photoUrl);

    // Create a Firebase account and get the custom auth token
    const firebaseToken = await createFirebaseAccount(
      userUID,
      fullname,
      photoUrl,
      emailAddress,
      linkedInAccessToken,
      vanityName,
    );
    functions.logger.info(`Firebase Token for UID '${userUID}' Token '${firebaseToken}'`);

    const configSnapshot = await db
      .collection('config')
      .doc('meta')
      .get();
    const localhost = configSnapshot.data().localhost;
    const redirect_url = localhost ? redirect_localhost : redirect_production;
    functions.logger.info('redirect_url: ', redirect_url);
    const tokenUrl = `${redirect_url}/${firebaseToken}`;
    return res.redirect(tokenUrl);
  } catch (error) {
    functions.logger.error('Generate Token Error: ', error);
    return res.jsonp({ error: error.toString });
  }
});

/**
 *
 * @param {*} linkedInAccessToken LinkedIn access token
 *
 * @returns {Promise} LinkedIn profile object
 */
async function getProfileObject(linkedInAccessToken) {
  const profileUrl = `${api_url}/me`;

  try {
    return await invoke(
      'GET',
      profileUrl,
      { 'Content-Type': 'Application/json' },
      {},
      {
        withAuth: true,
        access_token: linkedInAccessToken,
      },
    );
  } catch (error) {
    return functions.logger.error('Get LinkedIn Profile Error: ', error);
  }
}

/**
 *
 * @param {*} linkedInAccessToken LinkedIn access token
 *
 * @returns {Promise} LinkedIn vanity name object
 */
async function getVanityNameObject(linkedInAccessToken, userUID) {
  const vanityNameUrl = `${api_url}/people/(id:${userUID})`;

  try {
    return await invoke(
      'GET',
      vanityNameUrl,
      { 'Content-Type': 'Application/json' },
      {},
      {
        withAuth: true,
        access_token: linkedInAccessToken,
      },
    );
  } catch (error) {
    return functions.logger.error('Get LinkedIn Vanity Name Error: ', error);
  }
}

/**
 *
 * @param {*} linkedInAccessToken LinkedIn access token
 *
 * @returns {Promise} LinkedIn email object
 */
async function getEmailObject(linkedInAccessToken) {
  const emailUrl = `${api_url}/emailAddress?q=members&projection=(elements*(handle~))`;

  try {
    return await invoke(
      'GET',
      emailUrl,
      { 'Content-Type': 'Application/json' },
      {},
      {
        withAuth: true,
        access_token: linkedInAccessToken,
      },
    );
  } catch (error) {
    return functions.logger.error('Get LinkedIn Email Object Error: ', error);
  }
}

/**
 *
 * @param {*} linkedInAccessToken LinkedIn access token
 *
 * @returns {Promise} LinkedIn photo object
 */
async function getPhotoObject(linkedInAccessToken) {
  const photoQueryUrl = `${api_url}/me?projection=(id,profilePicture(displayImage~:playableStreams))`;

  try {
    return await invoke(
      'GET',
      photoQueryUrl,
      { 'Content-Type': 'Application/json' },
      {},
      {
        withAuth: true,
        access_token: linkedInAccessToken,
      },
    );
  } catch (error) {
    return functions.logger.error('Get LinkedIn Photo Object Error: ', error);
  }
}

/**
 * Invokes the given rest URL endpoint with body and headers
 *
 * @param {string} method - The HTTP verb (i.e. GET/POST)
 * @param {string} url - The uri endpoint for the HTTP service
 * @param {object} headers - The HTTP headers (i.e. {'random-header-name': 'random-header-value', 'content-type': 'application/json'})
 * @param {object} reqBody - The JSON data to POST if applicable, or null
 * @param {object} auth - An object to pass to make a call which requires authorization, example { withAuth: true, access_token: 'access_token' }
 *
 * @returns {object} The body of the HTTP response
 */
function invoke(
  method,
  url,
  headers = { 'Content-Type': 'Application/json' },
  reqBody = {},
  auth = { withAuth: false, access_token: null },
) {
  return new Promise((resolve, reject) => {
    let options;
    try {
      options = generateOptions(method, url, headers, reqBody, auth);
    } catch (err) {
      return reject(err);
    }
    request(options, (error, response, body) => {
      if (error) return reject(error);
      if (response.statusCode === 404) return resolve(null);
      if (response.statusCode !== 200 && response.statusCode !== 201)
        return reject(new Error(response.statusCode + ' ' + response.statusMessage + ': ' + JSON.stringify(body)));
      try {
        const data = JSON.parse(body);
        return resolve(data);
      } catch (err) {
        return reject(err);
      }
    });
  });
}

/**
 * Generate options for the HTTP request
 *
 * @param {string} method - The HTTP verb (i.e. GET/POST)
 * @param {string} url - The uri endpoint for the HTTP service
 * @param {object} headers - The HTTP headers (i.e. {'random-header-name': 'random-header-value', 'content-type': 'application/json'})
 * @param {object} reqBody - The JSON data to POST if applicable, or null
 * @param {object} auth - An object to pass to make a call which requires authorization, example { withAuth: true, access_token: 'access_token' }
 *
 * @returns {object} The option for the HTTP request
 */
function generateOptions(method, url, headers, body, auth) {
  headers['X-Restli-Protocol-Version'] = '2.0.0';
  if (auth.withAuth) {
    if (!auth.access_token) throw new Error('Missing required "access_token" in the auth body');
    headers['Authorization'] = `Bearer ${auth.access_token}`;
  }
  const options = { url, method, headers };
  if (body) options['body'] = JSON.stringify(body);
  return options;
}

/**
 * Creates a Firebase account with the given user profile and returns a custom auth token allowing
 * signing-in this account. Also saves the accessToken to the Firestore at /users/$uid
 *
 * @param {*} linkedinID LinkedIn id of the user
 * @param {*} displayName Display name of the user
 * @param {*} photoURL Photo URl of the user
 * @param {*} email Email of the user
 * @param {*} accessToken Access Token for LinkedIn
 * @param {*} vanityName Vanity name of the user
 *
 */
async function createFirebaseAccount(linkedinID, displayName, photoURL, email, accessToken, vanityName) {
  let uid;
  try {
    const currentUser = await auth.getUserByEmail(email);
    if (currentUser) uid = currentUser.uid;
    else uid = `linkedin:${linkedinID}`;
    functions.logger.info('uid: ', uid);

    const userObj = { displayName: displayName, email: email };
    if (photoURL) userObj.photoURL = photoURL;
    await auth.updateUser(uid, userObj);

    // Save the access token to Firestore
    await db.doc(`/tokens/${uid}`).set({ access_token: accessToken });

    // Save the user to Firestore
    await db.doc(`/users/${uid}`).update({
      access_token: `tokens/${uid}`,
      linkedin_id: linkedinID,
      isParticipant: true,
      user_name: displayName,
      photo_url: photoURL,
      profile_url: vanityName ? `https://linkedin.com/in/${vanityName}` : null,
      updatedAt: FieldValue.serverTimestamp(),
    });

    // Create a Firebase custom auth token.
    return await auth.createCustomToken(uid);
  } catch (error) {
    // If user does not exists we create it
    // functions.logger.error('Create Firebase Account Error: ', error);
    // functions.logger.error('Create Firebase Account Error Code: ', error.code);

    if (error.code === 'auth/user-not-found') {
      uid = `linkedin:${linkedinID}`;
      functions.logger.info('uid: ', uid);

      await db.doc(`/tokens/${uid}`).set({ access_token: accessToken });

      await db.doc(`/users/${uid}`).set({
        access_token: `tokens/${uid}`,
        linkedin_id: linkedinID,
        isParticipant: true,
        user_name: displayName,
        photo_url: photoURL,
        profile_url: vanityName ? `https://linkedin.com/in/${vanityName}` : null,
        score: 0,
        createdAt: FieldValue.serverTimestamp(),
        updatedAt: FieldValue.serverTimestamp(),
      });

      const userObj = { uid: uid, displayName: displayName, email: email };
      if (photoURL) userObj.photoURL = photoURL;
      const userRecord = await auth.createUser(userObj);
      // functions.logger.info('userRecord: ', userRecord);

      await auth.setCustomUserClaims(uid, { participant: true });

      // Create a Firebase custom auth token.
      return await auth.createCustomToken(uid);
    }

    if (error.code === 5 || error.code === '5') {
      functions.logger.info('uid: ', uid);

      await db.doc(`/tokens/${uid}`).set({ access_token: accessToken });

      await db.doc(`/users/${uid}`).set({
        access_token: `tokens/${uid}`,
        linkedin_id: linkedinID,
        user_name: displayName,
        photo_url: photoURL,
        profile_url: vanityName ? `https://linkedin.com/in/${vanityName}` : null,
        score: 0,
        createdAt: FieldValue.serverTimestamp(),
        updatedAt: FieldValue.serverTimestamp(),
      });

      // Create a Firebase custom auth token.
      return await auth.createCustomToken(uid);
    }
  }
}

/**
 * Get the full name of the user from the profileObject
 *
 * @param {*} profileObject profileObject retrieved from LinkedIn
 *
 * @returns {String} Full name of the user
 */
function getFullname(profileObject) {
  if (profileObject.firstName && profileObject.lastName) {
    const firstLocale = profileObject.firstName.preferredLocale;
    const firstPreferred = `${firstLocale.language}_${firstLocale.country}`;
    const first = profileObject.firstName.localized[firstPreferred];

    const lastLocale = profileObject.lastName.preferredLocale;
    const lastPreferred = `${lastLocale.language}_${lastLocale.country}`;
    const last = profileObject.lastName.localized[lastPreferred];

    return `${first} ${last}`;
  }
  return null;
}

/**
 * Get the vanity name of the user from the vanityNameObject
 *
 * @param {*} vanityNameObject vanityNameObject retrieved from LinkedIn
 *
 * @returns {String} Vanity name of the user
 */
function getVanityName(vanityNameObject) {
  if (isRealValue(vanityNameObject) && vanityNameObject.vanityName) return vanityNameObject.vanityName;
  return null;
}

/**
 * Verify whether an object is valid
 *
 * @param {*} obj Input object for verification
 *
 * @returns {Boolean} Whether the object is valid
 */
function isRealValue(obj) {
  return obj && obj !== 'null' && obj !== 'undefined';
}

/**
 * Get the email address of the user from the emailObject
 *
 * @param {*} emailObject emailObject retrieved from LinkedIn
 *
 * @returns {String} Email of the user
 */
function getEmailAddress(emailObject) {
  if (isRealValue(emailObject) && emailObject.elements) {
    const element = emailObject.elements[0];
    if (isRealValue(element) && element['handle~']) {
      const handle = element['handle~'];
      if (isRealValue(handle) && handle.emailAddress) return handle.emailAddress;
    }
  }
  return null;
}

/**
 * Extract the photo URL from LinkedIn's payload
 *
 * @param {*} photoObject photoObject retrieved from LinkedIn
 *
 * @returns {String} Photo url of the user
 */
function extractPhotoUrl(photoObject) {
  if (isRealValue(photoObject)) {
    const profilePicture = photoObject.profilePicture;
    if (profilePicture) {
      const displayImage = profilePicture['displayImage~'];
      if (displayImage.elements && displayImage.elements.length !== 0) {
        for (let element of displayImage.elements) {
          if (!element.artifact.includes('profile-displayphoto-shrink_400_400')) continue;
          return element.identifiers[0].identifier;
        }
      }
    }
  }
  return null;
}
