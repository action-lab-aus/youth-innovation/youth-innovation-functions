const functions = require('firebase-functions');
const environment = functions.config();
const function_url = environment.service.function_url;
const admin = require('firebase-admin');
const FieldValue = admin.firestore.FieldValue;
const db = admin.firestore();
const auth = admin.auth();
const BUCKET_NAME = 'youth-innovation';
const uuid = require('uuid');
const { generateLink } = require('./anonymous');
const bucket = admin.storage().bucket();
const axios = require('axios');
const fs = require('fs');
const os = require('os');
const path = require('path');
const xlsx = require('xlsx');
const { find } = require('lodash');
const countries = require('../utils/ifrclist.json');
const { shuffle, uniq } = require('lodash');
const AWS = require('aws-sdk');
const { getMediaConvertParams } = require('../utils/mediaConvertParams.js');
const { UserPropertyValue } = require('firebase-functions/lib/providers/analytics');
const _ = require('lodash');

/**
 * HTTP function for generating a judging allocation spreadsheet with magic links
 * for each allocation
 */
exports.createJudgingAllocation = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '2GB',
  })
  .https.onRequest(async (req, res) => {
    try {
      const lang = 'fr';
      // Get the maximum number of task per judging allocation
      const { judgingMax, phases, noOfJudges } = (await db.collection('config').doc('meta').get()).data();

      // Fetch all submissions with status `edited`
      const tmp = (
        await db
          .collection('submissions')
          .where('status', '==', 'edited')
          .where('phase', '==', '4')
          .where('language', '==', lang)
          // .where('language', 'in', ['ar', 'fr', 'es', 'en'])
          .get()
      ).docs;

      // Create the phase map for future query
      const phaseSections = {};
      phases.forEach(phase => {
        phaseSections[phase.code] = phase.noOfVideos;
      });

      // Create the criteria map for each phase
      const criteria = (await db.collection('judgingcriteria').get()).docs;
      const criteriaMap = {};
      criteria.forEach(item => {
        criteriaMap[`${item.data().phase}_${item.data().section}`] = item.ref;
      });

      // Shuffle the submission list
      let allocation = [];
      for (let i = 0; i < noOfJudges; i++) {
        const submissions = shuffle(tmp);
        const tmpAllocation = generateAllocationData(submissions, judgingMax, criteriaMap);
        allocation = [...allocation, ...tmpAllocation];
      }
      // functions.logger.info('allocation: ', allocation);

      // Generate secret code for each allocation and create magic links accordingly
      // Save the code and magic link into the allocation map, and create
      // `judgingallocation` objects in Firestore
      const allocationPromises = [];
      const anonymousPromises = [];
      const data = [];
      const allocationMapping = {};

      allocation.forEach(async item => {
        const code = uuid.v1();
        item.code = code;
        const { link, token } = generateLink(code);
        item.link = link;

        // Create worksheet entry for each allocation
        const tmp = [];
        for (let i = 0; i < item.submissions.length; i++) {
          const sub = item.submissions[i];
          tmp.push(find(countries, { code: sub.region }).name);
        }
        const regions = uniq(tmp);
        // functions.logger.info('Unique Regions: ', regions);

        data.push({
          regions: regions.join(', '),
          tag: item.tag,
          link: link,
          tasks: item.submissions.length,
        });
        // functions.logger.info('data: ', data);

        // Create allocation documents in Firestore
        const allocationRef = db.collection('judgingallocation').doc();
        const allocationId = allocationRef.id;
        allocationPromises.push(
          allocationRef.set({
            code: code,
            link: link,
            tasks: item.submissions,
            createdAt: FieldValue.serverTimestamp(),
          }),
        );

        for (let i = 0; i < item.submissions.length; i++) {
          const sid = item.submissions[i].submission.id;
          if (allocationMapping[sid])
            allocationMapping[sid].allocations.push({
              allocation_id: allocationId,
              task_index: i,
            });
          else {
            allocationMapping[sid] = {};
            allocationMapping[sid].allocations = [
              {
                allocation_id: allocationId,
                task_index: i,
              },
            ];
          }
        }

        // Create anonymous documents in Firestore
        const anonymousRef = db.collection('anonymous').doc(code);
        anonymousPromises.push(
          anonymousRef.set({
            token: token,
          }),
        );
      });

      // functions.logger.info('allocationMapping: ', allocationMapping);

      // functions.logger.info('allocation: ', JSON.stringify(allocation));
      await Promise.all(allocationPromises);
      await Promise.all(anonymousPromises);

      const allocationMapPromises = [];
      Object.keys(allocationMapping).forEach(sid => {
        allocationMapPromises.push(
          db.collection('judgingmapping').doc(sid).set({ allocations: allocationMapping[sid].allocations }),
        );
      });
      await Promise.all(allocationMapPromises);

      // Create a spreadsheet with judging allocation (grouping & magic links) and save it to
      // Firebase storage for sharing with National Societies
      const workbook = xlsx.utils.book_new();
      const worksheet = xlsx.utils.json_to_sheet(data);
      xlsx.utils.book_append_sheet(workbook, worksheet, 'judging_allocation');

      const exportFileName = `judging_allocation_${lang}.xlsx`;
      const tempFilePath = path.join(os.tmpdir(), exportFileName);
      xlsx.writeFile(workbook, tempFilePath);

      // Upload the worksheet to Firebase storgae
      await bucket.upload(tempFilePath, {
        destination: `judging/${exportFileName}`,
      });

      fs.unlinkSync(tempFilePath);
      res.status(200).send('Successfully created judging allocations...');
    } catch (error) {
      functions.logger.error(error);
      res.status(500).send('Error occurred when creating judging allocations...');
    }
  });

/**
 * HTTP function for generating an initial judging result spreadsheet with submission id,
 * tags, National Society, submission language, and judging result
 */
exports.createJudgingResultSheet = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '2GB',
  })
  .https.onRequest(async (req, res) => {
    try {
      // Get all judging allocations
      const startDate = new Date('2022-03-06');
      const judgingAllocation = await db.collection('judgingallocation').where('createdAt', '>=', startDate).get();
      const judgingAllocationPromises = [];

      // Fetch documents within the judging allocation collection
      judgingAllocation.docs.forEach(doc => {
        judgingAllocationPromises.push(db.collection('judgingallocation').doc(doc.id).get());
      });
      const allocations = await Promise.all(judgingAllocationPromises);

      // Group the allocations by submissions
      const submissionResults = {};
      for (let i = 0; i < allocations.length; i++) {
        const tasks = allocations[i].data().tasks;
        for (let j = 0; j < tasks.length; j++) {
          const sid = tasks[j].submission.id;
          const results = tasks[j].result;
          if (submissionResults[sid]) {
            submissionResults[sid].results.push(results);
          } else {
            submissionResults[sid] = {};
            submissionResults[sid].results = [results];
          }
        }
      }

      // Get submission details
      const submissionDetailPromises = [];
      Object.keys(submissionResults).forEach(sid => {
        submissionDetailPromises.push(db.doc(`submissions/${sid}`).get());
      });
      const submissionDeatil = await Promise.all(submissionDetailPromises);

      const judgingResultData = [];
      submissionDeatil.forEach(detail => {
        const { tags, region, language } = detail.data();

        let resultsStr = '';
        submissionResults[detail.id].results.forEach(result => {
          if (!result) resultsStr += '-1,-1,-1,';
          else resultsStr += `${result['0']},${result['1']},${result['2']},`;
        });

        judgingResultData.push({
          sid: detail.id,
          results: resultsStr,
          tags: tags.join(','),
          region: region,
          language: language,
        });
      });

      // Create a spreadsheet with judging results and save it to
      // Firebase storage for sharing with National Societies
      const workbook = xlsx.utils.book_new();
      const worksheet = xlsx.utils.json_to_sheet(judgingResultData);
      xlsx.utils.book_append_sheet(workbook, worksheet, 'judging_results_initial');

      const exportFileName = 'judging_results_initial.xlsx';
      const tempFilePath = path.join(os.tmpdir(), exportFileName);
      xlsx.writeFile(workbook, tempFilePath);

      // Upload the worksheet to Firebase storgae
      await bucket.upload(tempFilePath, {
        destination: `judging/${exportFileName}`,
      });

      fs.unlinkSync(tempFilePath);
      res.status(200).send('Successfully created judging results...');
    } catch (error) {
      functions.logger.error(error);
      res.status(500).send('Error occurred when creating judging results...');
    }
  });

exports.getSubmissionList = functions.https.onRequest(async (req, res) => {
  let media = await db.collection('media').where('type', '==', 'hd_video').get();
  let output = '';

  let submissionPromises = [];
  media.forEach(m => {
    const sid = m.data().submission;
    submissionPromises.push(db.doc(`${sid}`).get());
  });
  const submissions = await Promise.all(submissionPromises);

  // console.log(submissions);
  let userPromises = [];
  submissions.forEach(submission => {
    // console.log(submission.data().submitted_by);
    if (submission && submission.data() && submission.data().submitted_by) {
      const uid = submission.data().submitted_by.id;
      userPromises.push(auth.getUser(uid));
    }
  });

  let submissionMap = _.mapKeys(submissions, function (submission) {
    return submission.id;
    // if (submission && submission.data() && submission.data().submitted_by) return submission.data().submitted_by.id;
    // else return null;
  });

  submissionMap = _.mapValues(submissionMap, ff => {
    return ff.data() && ff.data().submitted_by ? ff.data().submitted_by.id : null;
  });

  console.log(submissionMap);

  const users = await Promise.allSettled(userPromises);

  // console.log(users);

  const emails = _.mapKeys(users, function (u) {
    return u.value.uid;
  });

  // console.log(emails);

  media.forEach(element => {
    let el = element.data();
    let email = emails[submissionMap[el.submission.replace('submissions/', '')]]
      ? emails[submissionMap[el.submission.replace('submissions/', '')]].value.email
      : 'unknown';
    output += `"${el.submission.replace('submissions/', '')}","https://youtube.com/v/${el.youtube}","${email}"\n`;
  });
  return res.send(output);
});

/**
 * HTTP function for generating a judging result spreadsheet with judging result
 * video, transcription, user name, and email
 */
exports.createJudgingResult = functions.https.onRequest(async (req, res) => {
  try {
    // Fetch all `judgingEditJobs` with status `edited`
    const tmp = (await db.collection('judgingeditjobs').where('status', '==', 'finalised').get()).docs;

    // Get all relevant submissions
    const submissionPromises = [];
    tmp.forEach(job => {
      submissionPromises.push(db.collection('submissions').doc(job.id).get());
    });
    const submissions = await Promise.all(submissionPromises);

    // Get all submission users
    const userPromises = [];
    submissions.forEach(submission => {
      const uid = submission.data().submitted_by.id;
      userPromises.push(auth.getUser(uid));
    });
    const users = await Promise.allSettled(userPromises);

    const data = [];
    for (let i = 0; i < tmp.length; i++) {
      const { src, transcript } = tmp[i].data();
      if (users[i].status === 'fulfilled')
        data.push({
          feedback_video: src,
          transcript: transcript,
          email: users[i].value.email,
          displayName: users[i].value.displayName,
        });
      else
        data.push({
          feedback_video: src,
          transcript: transcript,
          email: 'Unknown',
          displayName: 'Unknown',
        });
    }

    // Create a spreadsheet with judging results and save it to
    // Firebase storage for sharing with National Societies
    const workbook = xlsx.utils.book_new();
    const worksheet = xlsx.utils.json_to_sheet(data);
    xlsx.utils.book_append_sheet(workbook, worksheet, 'judging_results');

    const exportFileName = 'judging_results.xlsx';
    const tempFilePath = path.join(os.tmpdir(), exportFileName);
    xlsx.writeFile(workbook, tempFilePath);

    // Upload the worksheet to Firebase storgae
    await bucket.upload(tempFilePath, {
      destination: `judging/${exportFileName}`,
    });

    fs.unlinkSync(tempFilePath);
    res.status(200).send('Successfully created judging results...');
  } catch (error) {
    functions.logger.error(error);
    res.status(500).send('Error occurred when creating judging results...');
  }
});

/**
 * Triggered when a new feedback object has been created (with a reference to the src S3 URL of
 * the feedback video. Start the transcoding job, remove the submission from the judgingEditJobs
 * queue and start a new feedback editing job
 */
exports.newJudgingFeedbackTrigger = functions.firestore.document('media/{id}').onUpdate(async (change, context) => {
  const newValue = change.after.data() || {};
  const previousValue = change.before.data() || {};
  // functions.logger.info('previousValue.status: ', previousValue.status);
  // functions.logger.info('newValue.status: ', newValue.status);

  if (newValue.type !== 'feedback') return;
  if (newValue.status === previousValue.status) return;
  if (newValue.status !== 'edited') return;

  const mediaObj = newValue;
  functions.logger.info('mediaObj: ', mediaObj);

  try {
    // Parse and construct the AWS MediaConvert job params
    const url = mediaObj.src;
    const sid = mediaObj.submission.split('/')[1];
    const filePathArr = url.replace(`s3://${BUCKET_NAME}/`, '').split('/');
    const destindation = `s3://youth-innovation/submissions/${filePathArr[1]}/${filePathArr[2]}/`;
    const params = getMediaConvertParams(url, '_transcoded', destindation);

    // Create a promise on a MediaConvert object
    await new AWS.MediaConvert({ apiVersion: '2017-08-29' }).createJob(params).promise();

    // Remove the submission from the judgingEditJobs queue
    await db.collection('judgingeditjobs').doc(sid).update({
      status: 'edited',
    });

    const accepted = (await db.collection('judgingeditjobs').doc(sid).get()).data().accepted;

    // Start a new feedback editing job
    const judgingEditJobsSnap = await db
      .collection('judgingeditjobs')
      .where('status', '==', 'pending')
      .where('accepted', '==', accepted)
      .limit(1)
      .get();

    // Return if no item on the queue
    const judgingEditJobDoc = judgingEditJobsSnap.docs[0];
    if (!judgingEditJobDoc) return;

    const response = await axios.get(`${function_url}feedbackEdit/`, {
      params: { submission_id: judgingEditJobsSnap.docs[0].id },
    });
    functions.logger.info('Response: ', response);
  } catch (error) {
    functions.logger.error(error);
  }
});

/**
 * Generater submission allocation data
 *
 * @param {*} submissions Submission array
 * @param {*} judgingMax Maximum number of tasks per judging allocation
 */
function generateAllocationData(submissions, judgingMax, criteriaMap) {
  // Create the initial allocation array
  const allocation = [];
  const noOfArrs = Math.ceil((submissions.length * 3) / judgingMax);
  // functions.logger.info('noOfArrs: ', noOfArrs);

  for (let i = 0; i < noOfArrs; i++) {
    allocation[i] = {};
    const sub = submissions[i].data();

    allocation[i].submissions = [
      {
        submission: submissions[i].ref,
        section: 0,
        criteria: criteriaMap[`${sub.phase}_0`],
        region: sub.region,
      },
      {
        submission: submissions[i].ref,
        section: 1,
        criteria: criteriaMap[`${sub.phase}_1`],
        region: sub.region,
      },
      {
        submission: submissions[i].ref,
        section: 2,
        criteria: criteriaMap[`${sub.phase}_2`],
        region: sub.region,
      },
    ];
    const tags = Object.values(sub.tags);
    const tagIdx = Math.floor(Math.random() * tags.length);
    allocation[i].tag = tags[tagIdx];
  }

  // Loop through the remaining submission array
  // If similar tags exists in the allocation slot, add it into the allocation
  // Otherwise, loop through the allocation array until find a slot or create a new slot
  for (let i = noOfArrs; i < submissions.length; i++) {
    const sub = submissions[i];
    const tagsMap = sub.data().tags;
    const tags = Object.values(tagsMap);
    // functions.logger.info('tags: ', tags);

    let allocated = false;
    for (let k = 0; k < allocation.length; k++) {
      // functions.logger.info('allocation[k].tag: ', allocation[k].tag);
      if (tags.indexOf(allocation[k].tag) >= 0 && allocation[k].submissions.length < judgingMax) {
        allocation[k].submissions.push({
          submission: sub.ref,
          section: 0,
          criteria: criteriaMap[`${sub.data().phase}_0`],
          region: sub.data().region,
        });
        allocation[k].submissions.push({
          submission: sub.ref,
          section: 1,
          criteria: criteriaMap[`${sub.data().phase}_1`],
          region: sub.data().region,
        });
        allocation[k].submissions.push({
          submission: sub.ref,
          section: 2,
          criteria: criteriaMap[`${sub.data().phase}_2`],
          region: sub.data().region,
        });
        allocated = true;
        break;
      }
    }

    // If no match, create a new slot for allocation
    if (!allocated) {
      const newIdx = allocation.length;
      allocation[newIdx] = {};
      allocation[newIdx].submissions = [
        {
          submission: sub.ref,
          section: 0,
          criteria: criteriaMap[`${sub.data().phase}_0`],
          region: sub.data().region,
        },
        {
          submission: sub.ref,
          section: 1,
          criteria: criteriaMap[`${sub.data().phase}_1`],
          region: sub.data().region,
        },
        {
          submission: sub.ref,
          section: 1,
          criteria: criteriaMap[`${sub.data().phase}_2`],
          region: sub.data().region,
        },
      ];
      const tagIdx = Math.floor(Math.random() * tags.length);
      allocation[newIdx].tag = tags[tagIdx];
    }
  }
  return allocation;
}
