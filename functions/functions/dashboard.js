const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();
const _ = require('lodash');

exports.adminListSubmissions = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '1GB',
  })
  .https.onCall(async (data, context) => {
    // exports.adminListSubmissions = functions.https.onRequest(async (req, res) => {

    let user = (await db
      .collection('users')
      .doc(context.auth.uid)
      .get()).data();

    //Check that this user is actually an admin
    if (!_.includes(user.roles, 'admin')) {
      throw new Error('Not authenticated');
    }

    let pagesize = data.pagination.rowsPerPage || 0;
    let page = data.pagination.page || 0;
    let sort_by = data.pagination.sortBy || 'submitted_by';
    let sort_dir = data.pagination.descending || false;

    // Get all submissions matching the filter
    let snapshot = await db
      .collection('submissions')
      .orderBy('phase')
      .get();

    let submissions = [];
    snapshot.forEach(doc => {
      submissions.push(doc.data());
    });

    // Group by submitted_by reference
    let grouped = _.groupBy(submissions, 'submitted_by.id');

    if (pagesize === 0) pagesize = _.size(grouped);

    let newobj = [];

    // console.log(Object.keys(grouped));

    let allpromises = [];

    for (const key in grouped) {
      // console.log('key: ', key);
      // let user = null;

      // console.log(user.uid);

      let subs = grouped[key];
      subs = _.orderBy(subs, 'createdAt', 'desc');
      let first = _.first(subs);

      newobj.push({
        submitted_by: key,
        lastTouched: first.createdAt,
        region: first.region,
        language: first.language,
        commslanguage: first.commslanguage,
        submissions: subs,
      });
    }

    // console.log(newobj);

    newobj = _.orderBy(newobj, [sort_by, 'region', 'language'], [sort_dir ? 'asc' : 'desc', 'asc', 'asc']);

    // Send back requested page (and total page info)
    let skip = (page - 1) * pagesize;
    // functions.logger.info("skip: ", skip);
    // functions.logger.info("pagesize: ", pagesize);

    let therest = _.drop(newobj, skip);
    let paged = _.take(therest, pagesize);

    for (let p of paged) allpromises.push(admin.auth().getUser(p.submitted_by));
    let results = await Promise.allSettled(allpromises);

    let i = 0;
    paged.forEach(obj => {
      obj.submitted_by =
        results[i].status === 'fulfilled'
          ? results[i].value
          : {
              uid: 'x' + i,
              displayName: 'Unknown',
            };
      i++;
    });

    return {
      meta: {
        total: _.size(newobj),
        page,
        pagesize,
        descending: sort_dir,
        sortBy: sort_by,
      },
      data: paged,
    };
  });
