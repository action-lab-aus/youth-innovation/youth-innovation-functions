const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();
const FieldValue = admin.firestore.FieldValue;
const { default: axios } = require('axios');
const environment = functions.config();
const path = require('path');
const AWS = require('aws-sdk');
AWS.config.loadFromPath(path.join(__dirname, '/awsConfig.json'));
AWS.config.mediaconvert = { endpoint: environment.aws.mediaconvert_endpoint };
const { getMediaConvertParams, getMediaConvertParamsWithoutThumbnail } = require('../utils/mediaConvertParams.js');

/**
 * Format of request
 * {
 *    "date_updated":"2021-04-06T01:12:07.884735Z",
 *    "actions":["http://127.0.0.1/exports/16/download/","http://127.0.0.1/exports/16/retry/","http://127.0.0.1/exports/16/storage/"],
 *    "audio_bitrate":"1920000",
 *    "end_frame":"1",
 *    "export_type":"video",
 *    "video_codec":"libx264",
 *    "project":"http://127.0.0.1/projects/91/",
 *    "json":["hostname","file_size","status"],
 *    "url":"http://127.0.0.1/exports/16/",
 *    "id":"16",
 *    "video_bitrate":"8000000",
 *    "output":"http://127.0.0.1/media/video/output/91/output-91-16-4136c636.mp4",
 *    "start_frame":"1",
 *    "video_format":"mp4",
 *    "progress":"100.0",
 *    "date_created":"2021-04-06T01:12:07.479507Z",
 *    "audio_codec":"libfdk_aac",
 *    "webhook":"https://us-central1-youthinnovation-firebase.cloudfunctions.net/downloadEdit",
 *    "status":"completed"
 * }
 */

exports.editComplete = functions.https.onRequest(async (req, res) => {
  functions.logger.log(req.body);
  functions.logger.log(req.query);

  const snapshot = await db
    .collection('media')
    .doc(req.query.media_id)
    .get();

  // If the `hd_video` edit completes, trigger the transcoding service and
  // and create a new media object for streaming (when YouTube is not accessible)
  const { src, type, srcLang, submission } = snapshot.data();
  if (type === 'hd_video') {
    // Create media objects for the transcoded hd video
    const docRef = await db.collection('media').add({
      src: src.replace('.mp4', '_transcoded.mp4'),
      type: `hd_video_transcoded`,
      srcLang: srcLang,
      status: 'exporting',
      createdAt: FieldValue.serverTimestamp(),
      submission: submission,
    });

    // Add the transcoded media object id into the `hd_video` for future references
    await snapshot.ref.update({ transcodedId: docRef.id });

    // Append media object to the submission media array
    await db.doc(submission).update({
      media: admin.firestore.FieldValue.arrayUnion(docRef),
    });

    // Create MediaConvert request parameter
    const filePath = src.replace('hd_video.mp4', '');
    const params = getMediaConvertParamsWithoutThumbnail(src, '_transcoded', filePath);

    // Create a promise on a MediaConvert object
    await new AWS.MediaConvert({ apiVersion: '2017-08-29' }).createJob(params).promise();
  }

  // If the `sqaure_video` edit completes, trigger the transcoding service and
  // and create new media objects for the transcoded version & thumbnails
  if (type === 'square_video') {
    // Create media objects for the transcoded hd video
    const docRef = await db.collection('media').add({
      src: src.replace('.mp4', '_transcoded.mp4'),
      type: `square_video_transcoded`,
      srcLang: srcLang,
      status: 'exporting',
      createdAt: FieldValue.serverTimestamp(),
      submission: submission,
    });

    // Add the transcoded media object id into the `sqaure_video` for future references
    await snapshot.ref.update({ transcodedId: docRef.id });

    // Append media object to the submission media array
    await db.doc(submission).update({
      media: admin.firestore.FieldValue.arrayUnion(docRef),
    });

    // Create MediaConvert request parameter
    const filePath = src.replace('square_video.mp4', '');
    const params = getMediaConvertParams(src, '_transcoded', filePath);

    // Create a promise on a MediaConvert object
    await new AWS.MediaConvert({ apiVersion: '2017-08-29' }).createJob(params).promise();
  }

  // An array of promises that need to be completed before the function
  // finishes execution.
  let promises = [];

  const mediaObj = (await admin
    .firestore()
    .collection('media')
    .doc(req.query.media_id)
    .get()).data();

  // Update the status of the media object to be "edited"
  promises.push(
    admin
      .firestore()
      .collection('media')
      .doc(req.query.media_id)
      .update({ status: 'edited', createdAt: FieldValue.serverTimestamp() }),
  );

  // This block should only be executed if the media object isn't a feedback video
  // i.e. it's a hd_video, square_video etc.
  if (mediaObj.type !== 'feedback') {
    // We need to update the submission status. It is currently either "moderated" or "edited_[0-9]*"
    // Where moderated means no edits have been created yet, and the number in edited_[0-9]* is the number
    // of edits created so far.
    const submissionSnapshot = await admin
      .firestore()
      .doc(`submissions/${req.query.submission_id}`)
      .get();

    const submissionData = submissionSnapshot.data();

    const zeroIfNaN = val => (Number.isNaN(val) ? 0 : val);

    const editsCreatedSoFar = zeroIfNaN(
      submissionData.status === 'moderated' ? 0 : Number(submissionData.status.replace(/^\D+/g, '')),
    ); // This regex removes all leading non digits

    // We need to check if we've finished all the edits. If so, we need to set the status to "edited"
    const numberOfEditsToCreate = (await admin
      .firestore()
      .doc(`config/edit_templates`)
      .get()).data()[String(submissionData.phase)].length;

    promises.push(
      admin
        .firestore()
        .collection('submissions')
        .doc(req.query.submission_id)
        .update({
          status: numberOfEditsToCreate - 1 === editsCreatedSoFar ? 'edited' : `edited_${editsCreatedSoFar + 1}`,
        }),
    );
  }

  // Delete the openshot project
  promises.push(
    axios.request({
      url: `${environment.openshot.url}/projects/${req.query.project_id}/`,
      auth: {
        username: environment.openshot.username,
        password: environment.openshot.password,
      },
      method: 'DELETE',
    }),
  );

  await Promise.all(promises);
  res.status(200).send({});
});

/**
 * Pubsub function for updating editing status when both the `square_video` & `hd_video`
 * are completed at the same time and overwrite the index of each other
 */
exports.updateEditStatus = functions.pubsub.schedule('every 10 minutes').onRun(async context => {
  try {
    const submissions = await db
      .collection('submissions')
      .where('status', '==', 'edited_1')
      .get();

    const sids = [];
    submissions.forEach(sub => {
      sids.push(sub.id);
    });
    functions.logger.info('sids: ', sids);
    if (sids.length === 0) return;

    const promises = [];
    for (let i = 0; i < sids.length; i++) {
      promises.push(
        db
          .collection('media')
          .where('submission', '==', `submissions/${sids[i]}`)
          .where('type', 'in', ['square_video', 'hd_video'])
          .get(),
      );
    }

    const results = await Promise.all(promises);
    const submisssionMap = {};
    for (let i = 0; i < results.length; i++) {
      const result = results[i];
      result.forEach(res => {
        const { status, submission } = res.data();
        if (status === 'edited') {
          if (submisssionMap[submission]) submisssionMap[submission] += 1;
          else submisssionMap[submission] = 1;
        }
      });
    }

    const updatePromises = [];
    Object.keys(submisssionMap).forEach(key => {
      if (submisssionMap[key] === 2)
        updatePromises.push(
          db
            .collection('submissions')
            .doc(key.split('/')[1])
            .update({ status: 'edited' }),
        );
    });

    await Promise.all(updatePromises);
  } catch (error) {
    functions.logger.error(error);
  }
});
