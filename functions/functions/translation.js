const functions = require('firebase-functions');
const environment = functions.config();
const admin = require('firebase-admin');
const db = admin.firestore();
const FieldValue = admin.firestore.FieldValue;
const path = require('path');
const AWS = require('aws-sdk');
AWS.config.loadFromPath(path.join(__dirname, '/awsConfig.json'));
const translate = new AWS.Translate();
const { Translate } = require('@google-cloud/translate').v2;
const googleTranslate = new Translate({ key: environment.google.translation_api_key });
const MAX_BYTE = 4800;

/**
 * Triggered when new submission is received, create media objects for everything
 * under the formdata for automatic translation
 */
exports.newSubmissionTrigger = functions.firestore.document('submissions/{id}').onCreate(async (snap, context) => {
  const submission = snap.data();

  const identifiedlang = submission.language || 'en';
  functions.logger.info('identifiedlang: ', identifiedlang);
  const translationobjects = [];

  // Create translation objects based on the fields:
  for (let key in submission.formdata) {
    // Use the lang identified in the submission
    pushField(translationobjects, submission, key, identifiedlang, context.params.id);
  }

  // Map each translation object and create media objects accordingly
  const promises = translationobjects.map(async obj => {
    const { type, modality, langs, submission } = obj;

    // Add `langs` documents for each media object
    const newLangRef = db.collection('langs').doc();

    // Create the new media object
    const newMediaRef = db.collection('media').doc();
    langs['media'] = `media/${newMediaRef.id}`;
    await newLangRef.set(langs);

    await newMediaRef.set({
      type: type,
      modality: modality,
      langs: [newLangRef],
      submission: `submissions/${submission}`,
      createdAt: FieldValue.serverTimestamp(),
    });

    await snap.ref.update({
      media: admin.firestore.FieldValue.arrayUnion(newMediaRef),
    });
  });

  return await Promise.all(promises);
});

/**
 * Triggered when a submission has been moderated, update the media object
 * status to be `readyformanual`
 */
exports.onSubmissionModeratedTrigger = functions.firestore
  .document('submissions/{id}')
  .onUpdate(async (change, context) => {
    const newValue = change.after.data() || {};
    const previousValue = change.before.data() || {};
    const submission = change.after.data();

    // If it's not the same status as the last update, and its now 'moderated'
    if (newValue.status !== previousValue.status && newValue.status === 'moderated') {
      // Update all media objects (with text in them) to be 'readyformanualtranslate'
      const meidaPromises = [];
      const mediaRefs = submission.media;
      for (let i = 0; i < mediaRefs.length; i++) {
        meidaPromises.push(mediaRefs[i].get());
      }
      const results = await Promise.all(meidaPromises);

      const langsPromises = [];
      for (let i = 0; i < results.length; i++) {
        const data = results[i].data();
        if (data.langs && data.type !== 'raw_transcript') {
          for (let j = 0; j < data.langs.length; j++) {
            const langsRef = data.langs[j];
            langsPromises.push(langsRef.get());
          }
        }
      }
      const langsResult = await Promise.all(langsPromises);

      const updatePromises = [];
      for (let i = 0; i < langsResult.length; i++) {
        if (!langsResult[i].data().original)
          updatePromises.push(langsResult[i].ref.update({ status: 'readyformanualtranslate' }));
      }

      return await Promise.all(updatePromises);
    }
  });

/**
 * Triggered when a new media object has been created, call AWS Translate to automatically
 * translate the text contents into a list of target languages
 */
exports.newMediaObjectTrigger = functions.firestore.document('media/{id}').onCreate(async (snap, context) => {
  // Trigger the translation only when it is an original language
  const mediaObj = snap.data();
  if (!mediaObj.langs) return;

  const langRef = mediaObj.langs[0];
  const langSnapshot = await langRef.get();
  const langObj = langSnapshot.data();
  if (!langObj.original) return;

  const { srcLang, srcText } = langObj;
  const mediaType = mediaObj.type;

  const promises = [];
  const targetSnapshot = await db
    .collection('config')
    .doc('meta')
    .get();
  const targetCodes = targetSnapshot.data().target_language;

  for (let targetCode of targetCodes) {
    if (targetCode !== srcLang) {
      try {
        // Only create English translations for non-static content
        if (!mediaObj.type.startsWith('static_') && targetCode !== 'en') return;
        // No need for translation if the submission is in English
        if (!mediaObj.type.startsWith('static_') && srcLang === 'en') return;

        functions.logger.info(`Doing translation from ${srcLang} into ${targetCode}`);
        const newLangRef = db.collection('langs').doc();

        // Calculate the byte length for the source text
        const byteLength = Buffer.from(srcText).length;
        functions.logger.info(`byteLength: `, byteLength);

        let translatedText = '';
        // If media type starts with `static_`, create translation jobs instead of calling
        // the AWS real-time translate immediately to avoid the rate limit
        if (mediaObj.type.startsWith('static_')) {
          // If target language is Persian, use Google Translate for the translation
          if (targetCode === 'pa') {
            const [translation] = await googleTranslate.translate(srcText, 'pa');
            translatedText = translation;
          }
          // Else, push the `transJob` queue in Firestore
          else {
            const newTransJobRef = db.collection('transJob').doc();
            await newTransJobRef.set({
              lang: newLangRef.id,
              srcText: srcText,
              targetLang: targetCode,
              index: 0,
              mediaType: mediaType,
            });
          }
        } else {
          // For non-static content, if the srcLang is Punjabi, use Google translate to
          // translate the content into English
          if (srcLang === 'pa') {
            const [translation] = await googleTranslate.translate(srcText, 'en');
            translatedText = translation;
          } else {
            // If the source text is less than 4,800 bytes (maximum 5,000)
            if (byteLength <= MAX_BYTE) {
              // If there is a non-english piece of content, create the param
              let params = {
                SourceLanguageCode: 'auto',
                TargetLanguageCode: targetCode,
                Text: srcText,
              };

              // Push to AWS Translate for auto translation
              let translation = await new Promise((resolve, reject) => {
                translate.translateText(params, (err, data) => {
                  if (err) {
                    reject(err);
                  } else resolve(data);
                });
              });

              translatedText = translation.TranslatedText;
            } else {
              // Split the array into chunks for translation
              const strArr = chunk(srcText, MAX_BYTE);

              // Create new translation documents and push to Firestore
              // Since execute all translations in Promise.all will always generate Rate Exceeds
              // Exception, need to use a scheduler function for query and execute the queue items
              const promises = [];
              for (let i = 0; i < strArr.length; i++) {
                const newTransJobRef = db.collection('transJob').doc();
                promises.push(
                  newTransJobRef.set({
                    lang: newLangRef.id,
                    srcText: strArr[i],
                    targetLang: targetCode,
                    index: i,
                    mediaType: mediaType,
                  }),
                );
              }
              await Promise.all(promises);
            }
          }
        }

        functions.logger.info('translatedText: ', translatedText);

        // Push the new translation into promises
        const status = mediaType.indexOf('static_') >= 0 ? 'readyformanualtranslate' : 'autotranslated';
        const langObj = {
          srcText: srcText,
          srcLang: srcLang,
          targetText: translatedText,
          targetLang: targetCode,
          createdAt: FieldValue.serverTimestamp(),
          media: `media/${context.params.id}`,
        };

        // Set the status flag only when the source text is less than the maximum byte number
        if (mediaObj.type.startsWith('static_')) {
          langObj.targetLangArr = [null];
          langObj.description = mediaObj.description;
        } else {
          if (byteLength <= MAX_BYTE) langObj.status = status;
          else langObj.targetLangArr = Array(Math.ceil(byteLength / MAX_BYTE)).fill(null);
        }

        // Update status if the language pair contains Punjabi
        if (srcLang === 'pa' || targetCode === 'pa') langObj.status = status;

        promises.push(newLangRef.set(langObj));
        promises.push(
          snap.ref.update({
            langs: admin.firestore.FieldValue.arrayUnion(newLangRef),
          }),
        );
      } catch (err) {
        // e.g. UnsupportedLanguagePairException, ThrottlingException: Rate Exceeded
        functions.logger.error('Amazon Translate Error: ', err.message);
        // Set the flag for failed auto translations
        const newLangRef = db.collection('langs').doc();
        const status = mediaType.indexOf('static_') >= 0 ? 'readyformanualtranslate' : 'failedautotranslated';
        promises.push(
          newLangRef.set({
            srcText: srcText,
            srcLang: srcLang,
            status: status,
            error: `Amazon Translate Error: ${err.message}`,
            targetLang: targetCode,
            createdAt: FieldValue.serverTimestamp(),
            media: `media/${context.params.id}`,
          }),
        );

        promises.push(
          snap.ref.update({
            langs: admin.firestore.FieldValue.arrayUnion(newLangRef),
          }),
        );
      }
    }
  }

  return await Promise.all(promises);
});

/**
 * Firebase scheduler function which will be execueted every minute to fetch items from the
 * translation queue, and call AWS Translate to get translations
 */
exports.onTransJobCreatedScheduler = functions.pubsub.schedule('every 1 minutes').onRun(async context => {
  // exports.onTransJobCreatedScheduler = functions.https.onRequest(async (req, res) => {
  // Fetch the first object from the `transJob` queue
  const transJobSnapshot = await db
    .collection('transJob')
    .limit(1)
    .get();

  // Return if no item on the queue
  const transJobDoc = transJobSnapshot.docs[0];
  if (!transJobDoc) return;

  const transJobObj = transJobDoc.data();
  const { lang, srcText, targetLang, index, mediaType } = transJobObj;

  try {
    // Construct the AWS Translate parameter
    let params = {
      SourceLanguageCode: 'auto',
      TargetLanguageCode: targetLang,
      Text: srcText,
    };

    // Push to AWS Translate for auto translation
    const translation = await translate.translateText(params).promise();
    const translatedText = translation.TranslatedText;

    // Get and update the targetLangArr
    const snapshot = await db
      .collection('langs')
      .doc(lang)
      .get();

    // Fetch the target lanaguge array and update the text with the given index
    let targetLangArr = snapshot.data().targetLangArr;
    targetLangArr[index] = translatedText;
    await db
      .collection('langs')
      .doc(lang)
      .update({ targetLangArr: targetLangArr });

    // Remove the job on the `transJob` queue
    await transJobSnapshot.docs[0].ref.delete();

    // If all the jobs for a given lang object has finished, update the status and targetText
    if (targetLangArr.every(i => i !== null)) {
      const status = mediaType.indexOf('static_') >= 0 ? 'readyformanualtranslate' : 'autotranslated';

      await db
        .collection('langs')
        .doc(lang)
        .update({ targetText: targetLangArr.join(' '), status: status });
    }
  } catch (err) {
    functions.logger.error('Error: ', err.message);

    // Record error messages for the translation jobs
    const snapshot = await db
      .collection('langs')
      .doc(lang)
      .get();

    let targetLangArr = snapshot.data().targetLangArr;
    targetLangArr[index] = err.message;
    await db
      .collection('langs')
      .doc(lang)
      .update({ targetLangArr: targetLangArr });
    await transJobSnapshot.docs[0].ref.delete();

    if (targetLangArr.every(i => i !== null)) {
      const status = mediaType.indexOf('static_') >= 0 ? 'readyformanualtranslate' : 'failedautotranslated';
      await db
        .collection('langs')
        .doc(lang)
        .update({ targetText: targetLangArr.join(' '), status: status });
    }
  }
});

/**
 * Construct and push the media object into the array
 *
 * @param {*} array Array for storing media objects
 * @param {*} submission Submission received
 * @param {*} field Type of the content for translation
 * @param {*} lang Identified original language
 * @param {*} id Id of the submission
 */
function pushField(array, submission, field, lang, id) {
  const langs = {
    original: true,
    srcLang: lang,
    srcText: submission.formdata[field],
    createdAt: FieldValue.serverTimestamp(),
  };

  return array.push({
    type: field,
    modality: 'text',
    langs: langs,
    submission: id,
  });
}

/**
 * Divide the given string into an array of strings with the given maximum byte for
 * each chunk
 *
 * @param {*} str String to be splitted into chunks
 * @param {*} maxBytes Maximum byte for each chunk
 */
function chunk(str, maxBytes) {
  let buf = Buffer.from(str);
  const result = [];
  while (buf.length) {
    let i = buf.length <= maxBytes ? buf.length : buf.lastIndexOf(32, maxBytes + 1);
    // If no space found, try forward search
    if (i < 0) i = buf.indexOf(32, maxBytes);
    // If there's no space at all, take the whole string
    if (i < 0) i = buf.length;
    // This is a safe cut-off point; never half-way a multi-byte
    result.push(buf.slice(0, i).toString());
    buf = buf.slice(i + 1); // Skip space (if any)
  }
  return result;
}
