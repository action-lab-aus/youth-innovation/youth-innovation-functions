const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();
const _ = require('lodash');

/**
 * Scheduler function run every 10 minutes to update the score leaderboard
 * with the top 5 list & average score per person
 */
exports.updateLeaderboard = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '512MB',
  })
  .pubsub.schedule('every 20 minutes')
  .onRun(async context => {
    // exports.updateLeaderboard = functions.https.onRequest(async (req, res) => {
    const usersSnapshot = await db
      .collection('users')
      .where('roles', 'array-contains-any', ['transcriber', 'translator', 'verifier'])
      .get();
    const userCount = usersSnapshot.size;
    functions.logger.info('userCount: ', userCount);

    let sumScore = 0;
    let dailyScore = 0;
    const toplist = [];
    if (userCount === 0) return;

    // Fetch user scores in desc order
    const scoresSnapshot = await db
      .collection('users')
      .orderBy('score', 'desc')
      .get();

    // Get the top 5 scores and generate the top list
    for (let i = 0; i < scoresSnapshot.size; i++) {
      const data = scoresSnapshot.docs[i].data();
      if (data.score > 0) {
        sumScore += data.score;
        if (toplist.length < 5) toplist.push({ name: data.user_name, score: Math.round(data.score, 1) });
      } else break;
    }
    functions.logger.info('toplist: ', toplist);

    dailyScore = await getDailyScoreSum();
    functions.logger.info('dailyScore: ', dailyScore);

    const langs = await getLangCompleted();
    functions.logger.info('langs: ', langs);

    await db
      .collection('config')
      .doc('leaderboard')
      .update({
        perperson: Math.round(sumScore / userCount),
        perday: Math.round(dailyScore / userCount),
        toplist: toplist,
        langs: langs,
      });

    // return res.status(200).send('Successfully updated the leaderboard...');
  });

/**
 * Calculate the daily sum score based on the `verifiedAt` timestamp
 */
async function getDailyScoreSum() {
  let dailyScore = 0;

  // Get the score map from `config/meta`
  const scoreSnapshot = await db
    .collection('config')
    .doc('meta')
    .get();
  const scoreMap = scoreSnapshot.data().score_map;

  const current = new Date();
  current.setHours(current.getHours() - 24);
  const timestamp = admin.firestore.Timestamp.fromDate(current);

  // Fetch all language objects with status as `finalised` and verified within 24 hours
  const langsSnapshots = await db
    .collection('langs')
    .where('status', '==', 'finalised')
    .where('verifiedAt', '>=', timestamp)
    .get();

  const mediaPromises = [];
  langsSnapshots.forEach(doc => {
    // Add the verification score
    dailyScore += doc.data().score;

    // Push the get media query to the promise array
    mediaPromises.push(
      db
        .collection('media')
        .doc(doc.data().media.split('/')[1])
        .get(),
    );
  });
  const mediaResults = await Promise.all(mediaPromises);

  for (let i = 0; i < mediaResults.length; i++) {
    const mediaResult = mediaResults[i].data();
    dailyScore += scoreMap[mediaResult.type];
  }

  return dailyScore;
}

/**
 * Calculate the how many tasks from each language are left:
 */
async function getLangCompleted() {
  // Get every langs item
  const langs = await db
    .collection('langs')
    .where('status', 'in', ['readyforverify', 'readyformanualtranslate', 'finalised'])
    .get();

  let rawdata = [];
  langs.forEach(l => {
    rawdata.push(l.data());
  });

  // Group by lang
  const grouped = _.groupBy(rawdata, item => {
    return item.srcLang === 'en' ? item.targetLang : item.srcLang;
  });

  let result = [];

  // Count how many with status of finalised
  // For all media objects:
  // Get with type 'hd_video' and status: 'readyformanualsubtitle'
  const media = await db
    .collection('media')
    .where('type', '==', 'hd_video')
    .where('status', '==', 'readyformanualsubtitle')
    .get();

  rawdata = [];
  media.forEach(l => {
    rawdata.push(l.data());
  });

  // Group by srcLang
  const mediagrouped = _.groupBy(rawdata, 'srcLang');

  // For each lang
  for (k in grouped) {
    let media = 0;
    done = _.size(_.filter(grouped[k], { status: 'finalised' }));
    // If there are any uncompleted media, add them onto total
    if (mediagrouped[k]) media = _.size(mediagrouped[k]);
    if (k && k !== 'undefined')
      result.push({
        lang: k,
        completed: done / (_.size(grouped[k]) + media),
      });
  }

  return result;
}
