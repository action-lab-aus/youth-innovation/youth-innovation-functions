const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();
const axios = require('axios');
const environment = functions.config();
const { newMediaObj } = require('../utils/firebaseHelpers');
const { handle } = require('../utils/promiseHelpers.js');

const openshotLambdaUrl = `https://${environment.openshot_lambda.endpoint}`;

const judgementBaseProject = 2320; // The openshot project ID for the project with all of the feedback clip files

const createTimelineFromResults = async submissionId => {
  const judgingEditJobRef = db.collection('judgingeditjobs').doc(submissionId);

  const judgingEditJobSnapshot = await judgingEditJobRef.get();
  const judgingEditValues = judgingEditJobSnapshot.data();

  if (!judgingEditValues) throw new Error("Judging Edit Job Snapshot for this submission doesn't exist");

  const makeFeedbackTimelineObject = (questionNum, criteriaNum) => ({
    inBaseProject: true,
    name: `Q${questionNum}_C${criteriaNum}_F${judgingEditValues['q' + questionNum + '_c' + criteriaNum]}.mp4`,
  });

  const timeline = [
    {
      inBaseProject: true,
      name: 'main_title.mp4',
    },
    {
      inBaseProject: true,
      name: 'Intro1.mp4',
    },
    // Q1
    makeFeedbackTimelineObject(1, 1),
    makeFeedbackTimelineObject(1, 2),
    makeFeedbackTimelineObject(1, 3),
    // Q2
    makeFeedbackTimelineObject(2, 1),
    makeFeedbackTimelineObject(2, 2),
    makeFeedbackTimelineObject(2, 3),
    // Q3
    makeFeedbackTimelineObject(3, 1),
    makeFeedbackTimelineObject(3, 2),
    makeFeedbackTimelineObject(3, 3),
    { inBaseProject: true, name: 'Outro1.mp4' },
    {
      inBaseProject: true,
      name: 'main_title.mp4',
    },
  ];
  return timeline;
};

/**
 * Firebase function for creating a video that provides feedback for a users submission
 * based on the results of the judging for that submission.
 */
exports.feedbackEdit = functions.https.onRequest(async (req, res) => {
  functions.logger.log('req.body:', req.body);
  functions.logger.log('req.query: ', req.query);

  if (!req.query.submission_id) return res.status(400).send("Submission doesn't exist");

  const submissionId = req.query.submission_id;

  const submissionRef = db.collection('submissions').doc(submissionId);

  const submissionSnapshot = await submissionRef.get();

  functions.logger.log(submissionSnapshot);

  const submissionData = submissionSnapshot.data();

  if (!submissionData) return res.status(400).send("Submission doesn't exist");

  // Create the base project
  const [feedbackEditOpenshotProject, createFeedbackEditOpenshotProjectError] = await handle(
    axios.request({
      url: `${environment.openshot.url}/projects/${judgementBaseProject}/copy/`,
      method: 'POST',
      auth: {
        username: environment.openshot.username,
        password: environment.openshot.password,
      },
      data: {
        name: `${submissionId} Judge Edit`,
      },
    }),
  );

  if (createFeedbackEditOpenshotProjectError) {
    functions.logger.log(
      `Error creating feedback edit openshot project from base project ${judgementBaseProject}. (Submision ID: ${submissionId})`,
    );
    return res.status(400).send('Error creating feedback edit openshot project...');
  }

  const project = feedbackEditOpenshotProject.data;

  // Create the media object for this judgement's edit
  const [createFeedbackEditResult, createFeedbackEditMediaObjError] = await handle(
    newMediaObj('feedback', submissionSnapshot, 'feedback.mp4', submissionData.commslanguage),
  );

  if (createFeedbackEditMediaObjError) {
    functions.logger.log('Error creating firestore media object for the feedback video edit');
    functions.logger.log(createFeedbackEditMediaObjError);
    return res.status(400).send('Error creating firestore media object for the feedback video edit...');
  }

  const { exportPath: feedbackEditS3Path, newMediaId: feedbackEditMediaId } = createFeedbackEditResult;
  functions.logger.info('feedbackEditS3Path: ', feedbackEditS3Path);
  functions.logger.info('feedbackEditMediaId: ', feedbackEditMediaId);

  const [timelineResult, timelineError] = await handle(createTimelineFromResults(submissionId));

  if (timelineError) {
    functions.logger.warn(timelineError);
    return res.status(400).send(timelineError.message);
  }

  // Edit the feedback video
  const [createFeedbackEditResponse, createFeedbackEditError] = await handle(
    axios.request({
      url: openshotLambdaUrl,
      method: 'POST',
      data: {
        project: project.id,
        key: environment.openshot_lambda.secret,
        webhook: `${environment.service.function_url}editComplete?media_id=${feedbackEditMediaId}&submission_id=${
          submissionRef.id
        }&project_id=${project.id}`,
        bucket: 'youth-innovation',
        exportPath: feedbackEditS3Path,
        timeline: timelineResult,
      },
    }),
  );

  if (createFeedbackEditError) {
    functions.logger.error('createFeedbackEditError: ', createFeedbackEditError);
    return res.status(500).send(createFeedbackEditError);
  }

  functions.logger.log(createFeedbackEditResponse);

  return res.status(200).send('Success');
});
