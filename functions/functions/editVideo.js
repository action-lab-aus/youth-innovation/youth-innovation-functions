const functions = require('firebase-functions');
const axios = require('axios');
const path = require('path');
const environment = functions.config();
const url = `https://${environment.openshot_lambda.endpoint}`;
const key = environment.openshot_lambda.secret;
const { newMediaObj, getEdits } = require('../utils/firebaseHelpers');
const { handle } = require('../utils/promiseHelpers');

/**
 * Fill in the missing S3 paths and names for assets not in the base project.
 *
 * i.e. given a timeline that describes a video edit and a list of references
 * to firebase media objects that correspond to videos that should be in that edit,
 * populate the timeline with the correct S3 paths.
 *
 * The template stored in firebase doesn't have the S3 paths for videos not stored in the base project.
 * These need to be fetched based on the videos the user has uploaded for this submission.
 *
 * Given: populateTimeline([{inBaseProject: false}], [FirestoreSnapshotOfMediaObject])
 * we get: [{inBaseProject: false, name: 'Name of asset', S3Path: 'source path of asset'}]
 *
 * @param {object[]} skeletonTimeline - the timeline template as stored in the firebase (config/edit_templates)
 * @param {DocumentSnapshot[]} rawAssets - the firebase media objects for the raw videos associated with this submission
 * @returns {object[]} the populated timeline
 */
const populateTimeline = (skeletonTimeline, rawAssets, editType, language, phase) => {
  const translatedLanguages = [
    'ar',
    'bn',
    'en',
    'es',
    'fa',
    'fr',
    'hi',
    'id',
    'ja',
    'ko',
    'pa',
    'pt',
    'ru',
    'sw',
    'tr',
    'ur',
    'zh',
  ];

  /*
   * In firestore, src properties are stored like so:
   *  's3://youth-innovation/submissions/test_edit_pipeline/test_edit_pipeline/Test_Video.mp4'
   * In the below logic, we parse away the s3:// and the bucket name
   */
  const assetData = rawAssets.map(asset => {
    return {
      name: path.basename(asset.data().src),
      S3Path: asset
        .data()
        .src.split('/')
        .slice(3)
        .join('/'),
    };
  });

  let i = 0;
  return skeletonTimeline
    .map(timelineObj => {
      if (timelineObj === 'outro') {
        return {
          inBaseProject: true,
          name: 'outro_notext.mp4',
          json: {
            scale: editType === 'square_video' ? 0 : 1,
          },
          above: [
            {
              inBaseProject: true,
              name:
                translatedLanguages.indexOf(language) === -1
                  ? `${editType}_outro_en.png`
                  : `${editType}_outro_${language}.png`,
            },
          ],
        };
      }
      if (timelineObj.inBaseProject) {
        return {
          ...timelineObj,
          above:
            timelineObj.above &&
            timelineObj.above.map(el =>
              el.isTitle === true
                ? {
                    inBaseProject: true,
                    name:
                      translatedLanguages.indexOf(language) === -1
                        ? `${editType}_title_en_${phase}_${i + 1}.png`
                        : `${editType}_title_${language}_${phase}_${i + 1}.png`,
                  }
                : el,
            ),
          below:
            timelineObj.below &&
            timelineObj.below.map(el =>
              el.isTitle === true
                ? {
                    inBaseProject: true,
                    name:
                      translatedLanguages.indexOf(language) === -1
                        ? `${editType}_title_en_${phase}_${i + 1}.png`
                        : `${editType}_title_${language}_${phase}_${i + 1}.png`,
                  }
                : el,
            ),
        };
      }
      i++;
      // There should always be a corresponding raw asset for each 'inBaseProject === false'
      // timeline object. However, we check - and if there isn't, we set the value to undefined
      // and then filter out any undefined object
      return assetData[i - 1]
        ? {
            ...timelineObj,
            name: assetData[i - 1].name,
            S3Path: assetData[i - 1].S3Path,
          }
        : undefined;
    })
    .filter(el => el !== undefined);
};

exports.createEditFromTemplate = async (editTemplate, submissionSnapshot, submissionData, raw_docs) => {
  const [editMediaObjectResponse, editMediaObjectError] = await handle(
    newMediaObj(editTemplate.type, submissionSnapshot, `${editTemplate.type}.mp4`, submissionData.language),
  );
  if (editMediaObjectError) {
    functions.logger.log(`Error creating edit media object`);
    return editMediaObjectError;
  }

  const { exportPath, newMediaId, newMediaRef } = editMediaObjectResponse;

  const [createBaseProjectResponse, createBaseProjectError] = await handle(
    axios.request({
      url: `${environment.openshot.url}/projects/${editTemplate.baseProject}/copy/`,
      method: 'POST',
      auth: {
        username: environment.openshot.username,
        password: environment.openshot.password,
      },
      data: {
        name: submissionData.formdata.title,
      },
    }),
  );
  if (createBaseProjectError) {
    functions.logger.log(`Error copying base openshot project with id: ${editTemplate.baseProject}`);
    return createBaseProjectError;
  }
  const { data: project } = createBaseProjectResponse;

  functions.logger.log('New media Object id: ', newMediaId);
  functions.logger.log('New openshot project id: ', project.id);

  const [createEditResponse, createEditError] = await handle(
    axios.request({
      url: url,
      method: 'POST',
      data: {
        project: project.id,
        key: key,
        webhook: `${environment.service.function_url}editComplete?media_id=${newMediaId}&submission_id=${
          submissionSnapshot.id
        }&project_id=${project.id}`,
        youthInnovation: { mediaId: newMediaId },
        bucket: 'youth-innovation',
        exportPath: exportPath,
        timeline: populateTimeline(
          editTemplate.timeline,
          raw_docs,
          editTemplate.type,
          submissionData.language,
          submissionData.phase,
        ),
        soundtrack: editTemplate.soundtrack,
        overlay: editTemplate.overlay,
        dev:
          environment.service.function_url === 'https://us-central1-youthinnovation-firebase.cloudfunctions.net/'
            ? false
            : true,
      },
    }),
  );
  if (createEditError) {
    functions.logger.log('Error creating the openshot edit');
    functions.logger.log(createEditError);
    return createEditError;
  }
  await newMediaRef.update({ openshotId: project.id });
  return createEditResponse;
};

/**
 * When the submission status is changed to moderated,
 * we call the AWS Lambda endpoint with the name of the new
 * Project and each edit that needs to be created for the
 * moderated submission
 */
exports.editVideo = functions
  .runWith({
    timeoutSeconds: 540,
  })
  .firestore.document('submissions/{submissionId}')
  .onUpdate(async (change, context) => {
    const newData = change.after.data();

    if (change.before.data().status !== 'moderated' && newData.status === 'moderated') {
      functions.logger.log(`${newData.formdata.title} - ${context.params.submissionId} is now in moderated`);
      functions.logger.log(newData);

      // Get the media objects associated with this submission that are raw assets to be edited
      const raw_docs = await Promise.all(newData.media.map(media => media.get())).then(media_array =>
        media_array.filter(media => media.data().type === 'raw'),
      );

      // Get edits from firestore: config/edit_templates for this phase (each phase has an array of edits associated with it)
      const edits = await getEdits(newData.phase);
      functions.logger.log(edits);

      // Here, we create a new media object in the firestore for each edit
      // then, we call the aws lambda to create the edit
      const editResponses = await Promise.all(
        edits.map(async edit => exports.createEditFromTemplate(edit, change.after, newData, raw_docs)),
      );

      editResponses.forEach(async response => {
        functions.logger.info('response: ', response);
        if (!response || response.status > 299 || response.status < 200) {
          await change.after.ref.update({
            error: 'Creating an edit from your submission failed!',
            status: 'error',
          });
          if (response.response !== undefined && response.response.body) {
            functions.logger.log('Editing pipeline error message: ');
            functions.logger.log(response.response.body);
          }
        }
      });
    }

    return 200;
  });
