const functions = require('firebase-functions');
const environment = functions.config();
const { default: axios } = require('axios');
const { handle } = require('../utils/promiseHelpers');

// Function to restart any exports that have been stuck on 'pending' for more than an hour
exports.cleanupOpenshotExports = functions.pubsub.schedule('every 60 minutes').onRun(async () => {
  const [exportsResult, exportsError] = await handle(
    axios.request({
      url: `${environment.openshot.url}/exports/`,
      auth: {
        username: environment.openshot.username,
        password: environment.openshot.password,
      },
      method: 'GET',
    }),
  );

  console.log(exportsError);
  if (exportsError) return;

  const currentExports = exportsResult.data;

  console.log(currentExports);

  const promises = [];
  const ONE_HOUR = 60 * 60 * 1000;
  openshotExports.results.forEach(e => {
    const updatedAt = new Date(e.date_updated);
    if (e.status === 'in-progress' && new Date() - updatedAt > ONE_HOUR) {
      promises.push(
        axios.request({
          url: `${environment.openshot.url}/exports/${e.id}/`,
          auth: {
            username: environment.openshot.username,
            password: environment.openshot.password,
          },
          method: 'DELETE',
        }),
      );

      promises.push(
        axios.request({
          url: `${environment.openshot.url}/exports/`,
          auth: {
            username: environment.openshot.username,
            password: environment.openshot.password,
          },
          data: {
            json: e.json,
            webhook: e.webhook,
            video_bitrate: e.video_bitrate,
          },
          method: 'POST',
        }),
      );
    }
  });

  currentExports.results.forEach(e => {
    const updatedAt = new Date(e.date_updated);
    if (e.status === 'in-progress' && new Date() - updatedAt > ONE_HOUR) {
      console.log('Deleting Export:');
      console.log(e);
      promises.push(
        axios.request({
          url: `${environment.openshot.url}/exports/${e.id}/`,
          auth: {
            username: environment.openshot.username,
            password: environment.openshot.password,
          },
          method: 'DELETE',
        }),
      );
      promises.push(
        axios.request({
          url: `${environment.openshot.url}/exports/`,
          auth: {
            username: environment.openshot.username,
            password: environment.openshot.password,
          },
          data: {
            project: e.project.replace('http', 'https'),
            json: {
              bucket: e.json.bucket,
              url: e.json.url,
              acl: 'private',
            },
            webhook: e.webhook,
            video_bitrate: e.video_bitrate,
          },
          method: 'POST',
        }),
      );
    }
  });

  await Promise.all(promises);
});
