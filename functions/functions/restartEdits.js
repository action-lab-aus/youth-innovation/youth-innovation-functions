const functions = require('firebase-functions');
const environment = functions.config();
const admin = require('firebase-admin');
const db = admin.firestore();
const { createEditFromTemplate } = require('./editVideo');
const { getEdits } = require('../utils/firebaseHelpers');
const { default: axios } = require('axios');

const THIRTY_MINUTES = 60 * 60 * 1000;

exports.restartSubmissionEdits = functions.pubsub.schedule('every 12 hours').onRun(async () => {
  const ref = db
    .collection('media')
    .where('status', '==', 'exporting')
    .where('type', 'in', ['hd_video', 'square_video']);
  const data = await ref.get();
  const promises = data.docs.map(async doc => {
    const mediaData = doc.data();

    // If the media object was created less than thirty minutes ago,
    // don't restart it (it's possible it is still editing)
    if (new Date() - mediaData.createdAt.toDate() <= THIRTY_MINUTES) {
      return;
    }

    const submissionSnapshot = await db.doc(mediaData.submission).get();
    const submissionData = submissionSnapshot.data();

    // Get the media objects associated with this submission that are raw assets to be edited
    const raw_docs = await Promise.all(submissionData.media.map(media => media.get())).then(media_array =>
      media_array.filter(media => media.data().type === 'raw'),
    );

    // Get edits from firestore: config/edit_templates for this phase (each phase has an array of edits associated with it)
    const edits = (await getEdits(submissionData.phase)).filter(edit => edit.type === mediaData.type);

    // If we don't find the corresponding edit - return
    if (edits[0] === undefined) return;

    if (mediaData.openshotId) {
      try {
        await axios.request({
          url: `${environment.openshot.url}/projects/${mediaData.openshotId}/`,
          method: 'DELETE',
          auth: {
            username: environment.openshot.username,
            password: environment.openshot.password,
          },
        });
      } catch (e) {
        functions.logger.log(e);
        functions.logger.log(`Failed to delete old project (with id: ${mediaData.openshotId}`);
      }
    }

    return createEditFromTemplate(edits[0], submissionSnapshot, submissionData, raw_docs).then(async _ => {
      return Promise.all([
        doc.ref.delete(),
        submissionSnapshot.ref.update({
          media: admin.firestore.FieldValue.arrayRemove(doc.ref),
        }),
      ]);
    });
  });
  await Promise.all(promises);
  return;
});
