const functions = require('firebase-functions');
const admin = require('firebase-admin');
const auth = admin.auth();
const { Storage } = require('@google-cloud/storage');
const gcs = new Storage();
const sharp = require('sharp');
const fs = require('fs-extra');
const os = require('os');
const path = require('path');
const uuid = require('uuid');

/**
 * On receiving profile images, resize the image using sharp: https://www.npmjs.com/package/sharp
 */
exports.resizeProfileImages = functions.storage.object().onFinalize(async object => {
  try {
    // Generate a unique name we'll use for the temp directories
    const uniqueName = uuid.v1();

    // Get the bucket original image was uploaded to
    const bucket = gcs.bucket(object.bucket);

    // Set up bucket directory
    const filePath = object.name;
    const fileName = filePath.split('/').pop();
    const bucketDir = path.dirname(filePath);

    // Create some temp working directories to process images
    const workingDir = path.join(os.tmpdir(), `images_${uniqueName}`);
    const tmpFilePath = path.join(workingDir, `source_${uniqueName}.jpg`);

    // Return if the file is a Firestore backup
    if (!filePath.startsWith('users')) return;

    // We don't want to process images already resized
    if (fileName.includes('image@') || !object.contentType.includes('image')) return false;

    // Ensure directory exists
    await fs.ensureDir(workingDir);

    // Download source file
    await bucket.file(filePath).download({
      destination: tmpFilePath,
    });

    // Resize images
    const size = 128;
    const thumbName = `image@${size}_${fileName}`;
    const thumbPath = path.join(workingDir, thumbName);

    // Square aspect ratio, good for profile images
    await sharp(tmpFilePath)
      .resize(size, size)
      .toFile(thumbPath);

    // Upload to original bucket
    await bucket.upload(thumbPath, {
      destination: path.join(bucketDir, thumbName),
    });

    // Get the signed url of the resized image
    const url = await bucket
      .file(path.join(bucketDir, thumbName))
      .getSignedUrl({ action: 'read', expires: '03-17-2059' });
    functions.logger.info('Signed URL: ', url[0]);

    const uid = bucketDir.split('/')[1];
    functions.logger.info('uid: ', uid);
    await auth.updateUser(uid, { photoURL: url[0] });

    // Remove the temp directories
    await fs.remove(workingDir);
    await fs.remove(bucketDir);

    return Promise.resolve();
  } catch (error) {
    // If we have an error, return it
    return Promise.reject(error);
  }
});
