#!/usr/bin/env node
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
require('deepdash')(_);
const xlsx = require('xlsx');

yargs(hideBin(process.argv))
  .usage('Usage: $0 <command> [options]')
  .command(
    'export',
    'Exports Vue json locale files into xlsx file',
    yargs => {
      yargs
        .option('input', {
          alias: 'i',
          type: 'string',
          description: 'locales dir to import',
        })
        .option('reference', {
          alias: 'r',
          type: 'string',
          description: 'reference name to include in translation',
        })
        .option('lang', {
          alias: 'l',
          type: 'string',
          description: 'language to use as source text',
          default: 'en',
        })
        .option('output', {
          alias: 'o',
          type: 'string',
          description: 'output filename',
          default: 'locales.xlsx',
        })
        .demandOption(['input', 'reference']);
    },
    argv => {
      exportFile(argv);
    },
  )
  .command(
    'import',
    'Imports translations from an xlsx file into Vue json locale files',
    yargs => {
      yargs
        .option('input', {
          alias: 'i',
          type: 'string',
          description: 'xlsx file to import',
        })
        .option('reference', {
          alias: 'r',
          type: 'string',
          description: 'reference name used to export originally',
        })
        .option('lang', {
          alias: 'l',
          type: 'string',
          description: 'language to use as source text (i.e. keep)',
          default: 'en',
        })
        .option('output', {
          alias: 'o',
          type: 'string',
          description: 'dir of target locale files',
        })
        .demandOption(['input', 'reference', 'output']);
    },
    argv => {
      importFile(argv);
    },
  )
  .demandCommand()
  .help('h')
  .alias('help', 'h').argv;

function importFile(argv) {
  // Load xlsx file
  const buffer = fs.readFileSync(path.resolve(argv.input));
  const workbook = xlsx.read(buffer, { type: 'buffer' });
  const sheetNameList = workbook.SheetNames;
  const xlData = xlsx.utils.sheet_to_json(workbook.Sheets[sheetNameList[0]]);
  // console.log("xlData: ", xlData);

  let outputFiles = {};
  let cols = _.without(_.keys(xlData[0]), 'type', 'content', 'description', argv.lang);
  // console.log('cols: ', cols);

  //for each row:
  for (let row of xlData) {
    if (row.type === 'static_locale') {
      // For each column in the xlsx, if its not lang, then write it to the locales dir.
      for (let locale of cols) {
        let key = `${locale}.${row.content.replace(`${argv.reference}.`, '')}`;
        if (row[locale]) _.set(outputFiles, key, row[locale]);
        else {
          if (key.split('.').length >= 3) _.set(outputFiles, key, row[argv.lang]);
        }
      }
    }
  }

  // Write output files:
  for (let locale of _.keys(outputFiles)) {
    let fpath = `${path.resolve(argv.output)}/${locale}.json`;
    // console.log("fpath: ", fpath);
    fs.writeFileSync(fpath, JSON.stringify(outputFiles[locale], null, 2));
  }

  console.info('Import Completed, locale files updated!');
}

function exportFile(argv) {
  let localespath = path.resolve(argv.input);
  let source = `${localespath}/${argv.lang}.json`;

  let sourceText = require(source);

  // Get all rest of langs that we want:
  let langs = _.sortBy(
    _.without(
      _.map(fs.readdirSync(localespath), f => {
        return f.replace('.json', '');
      }),
      argv.lang,
    ),
  );

  let output = [];

  let paths = _.paths(sourceText, {
    pathFormat: 'string',
  });

  for (const k of paths) {
    let row = {
      type: 'static_locale',
      content: `${argv.reference}.${k}`,
      description: `From ${localespath}`,
    };

    row[argv.lang] = _.get(sourceText, k);

    for (let l of langs) {
      row[l] = '';
    }

    output.push(row);
  }

  let workbook = xlsx.utils.book_new();
  let sheet = xlsx.utils.json_to_sheet(output);
  xlsx.utils.book_append_sheet(workbook, sheet, 'staticComms');

  xlsx.writeFile(workbook, `${argv.output}`);

  console.info(`File output to ${argv.output} complete!`);
}
