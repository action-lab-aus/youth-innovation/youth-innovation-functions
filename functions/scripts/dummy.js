// const admin = require("firebase-admin");
// admin.initializeApp();
const firebase = require("firebase/app");
require("firebase/auth");
require("firebase/firestore");

const _ = require("lodash");

const loremIpsum = require("lorem-ipsum").loremIpsum;

// const firestore = require("firebase/firestore");

firebase.initializeApp({ projectId: "youthinnovation-firebase" });

// const firebase = require('@google-cloud/firestore');

var db = firebase.firestore();

db.useEmulator("localhost", 8080);

//create dummy content:

let NUMBEROFENTRIES = 100;
let ORIGINALLANGS = ["en", "es", "ar", "fr"];

const dothis = async function() {
  for (let i = 0; i < NUMBEROFENTRIES; i++) {
    let langs = {};

    langs[_.sample(ORIGINALLANGS)] = {
      text: loremIpsum({
        units: "words",
        count: 5 + Math.round(Math.random() * 10),
      }),
      original: true,
    };

    let obj = {
      langs,
      modality: "text",
      type: "title",
    };

    await db.collection("media").add(obj);
    console.log(".");
  }
};

dothis();
