const functions = require('firebase-functions');
const environment = functions.config();
const admin = require('firebase-admin');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(environment.sendgrid.api_key);
const SENDGRID_SENDER = environment.sendgrid.sender;
const CHALLENGE_ID = environment.sendgrid.early_submission_challenge_id;
const IDEA_ID = environment.sendgrid.early_submission_idea_id;
const FINAL_ID = environment.sendgrid.final_submission_id;
const RESUBMISSION_ID = environment.sendgrid.resubmission_id;
const CONFIRMATION_ID = environment.sendgrid.confirmation_id;

/**
 * Send notification email to the participant when the video has been submitted
 *
 * @param {*} uid User ID
 * @param {*} commsLang Preferred communication language
 */
async function sendSubmitConfirmationEmail(uid, commsLang) {
  const userRecord = await admin.auth().getUser(uid);
  const userEmail = userRecord.email;
  const userName = userRecord.displayName;
  console.log('userEmail: ', userEmail);

  const msg = {
    to: userEmail,
    from: SENDGRID_SENDER,
    templateId: CONFIRMATION_ID,
    dynamic_template_data: {
      name: userName,
      english: commsLang === 'en' ? true : false,
      french: commsLang === 'fr' ? true : false,
      spanish: commsLang === 'es' ? true : false,
      arabic: commsLang === 'ar' ? true : false,
      bengali: commsLang === 'bn' ? true : false,
      persian: commsLang === 'fa' ? true : false,
      bahasa: commsLang === 'id' ? true : false,
      japanese: commsLang === 'ja' ? true : false,
      hindi: commsLang === 'hi' ? true : false,
      punjabi: commsLang === 'pa' ? true : false,
      portuguese: commsLang === 'pt' ? true : false,
      turkish: commsLang === 'tr' ? true : false,
      russian: commsLang === 'ru' ? true : false,
      urdu: commsLang === 'ur' ? true : false,
      chinese: commsLang === 'zh' ? true : false,
      korean: commsLang === 'ko' ? true : false,
      swahili: commsLang === 'sw' ? true : false,
    },
  };

  const response = await sgMail.send(msg);
  console.log(response[0].statusCode);
}

/**
 * Send notification email to the participant when the video finishes the
 * automatic transcoding, transcription, and editing
 *
 * @param {*} uid User ID
 * @param {*} commsLang Preferred communication language
 * @param {*} phase Phase for the submission
 */
async function sendEditSuccessEmail(uid, commsLang, phase) {
  const userRecord = await admin.auth().getUser(uid);
  const userEmail = userRecord.email;
  const userName = userRecord.displayName;
  console.log('userEmail: ', userEmail);

  const msg = {
    to: userEmail,
    from: SENDGRID_SENDER,
    templateId: phase === '1' ? CHALLENGE_ID : phase === '2' ? IDEA_ID : FINAL_ID,
    dynamic_template_data: {
      name: userName,
      english: commsLang === 'en' ? true : false,
      french: commsLang === 'fr' ? true : false,
      spanish: commsLang === 'es' ? true : false,
      arabic: commsLang === 'ar' ? true : false,
      bengali: commsLang === 'bn' ? true : false,
      persian: commsLang === 'fa' ? true : false,
      bahasa: commsLang === 'id' ? true : false,
      japanese: commsLang === 'ja' ? true : false,
      hindi: commsLang === 'hi' ? true : false,
      punjabi: commsLang === 'pa' ? true : false,
      portuguese: commsLang === 'pt' ? true : false,
      turkish: commsLang === 'tr' ? true : false,
      russian: commsLang === 'ru' ? true : false,
      urdu: commsLang === 'ur' ? true : false,
      chinese: commsLang === 'zh' ? true : false,
      korean: commsLang === 'ko' ? true : false,
      swahili: commsLang === 'sw' ? true : false,
    },
  };

  const response = await sgMail.send(msg);
  console.log(response[0].statusCode);
}

/**
 * Send notification email to the participant when the video processing
 * encounters an error and need to notify the user for resubmission
 *
 * @param {*} uid User ID
 * @param {*} commsLang Preferred communication language
 */
async function sendResubmitEmail(uid, commsLang) {
  const userRecord = await admin.auth().getUser(uid);
  const userEmail = userRecord.email;
  const userName = userRecord.displayName;
  console.log('userEmail: ', userEmail);

  const msg = {
    to: userEmail,
    from: SENDGRID_SENDER,
    templateId: RESUBMISSION_ID,
    dynamic_template_data: {
      name: userName,
      english: commsLang === 'en' ? true : false,
      french: commsLang === 'fr' ? true : false,
      spanish: commsLang === 'es' ? true : false,
      arabic: commsLang === 'ar' ? true : false,
      bengali: commsLang === 'bn' ? true : false,
      persian: commsLang === 'fa' ? true : false,
      bahasa: commsLang === 'id' ? true : false,
      japanese: commsLang === 'ja' ? true : false,
      hindi: commsLang === 'hi' ? true : false,
      punjabi: commsLang === 'pa' ? true : false,
      portuguese: commsLang === 'pt' ? true : false,
      turkish: commsLang === 'tr' ? true : false,
      russian: commsLang === 'ru' ? true : false,
      urdu: commsLang === 'ur' ? true : false,
      chinese: commsLang === 'zh' ? true : false,
      korean: commsLang === 'ko' ? true : false,
      swahili: commsLang === 'sw' ? true : false,
    },
  };

  const response = await sgMail.send(msg);
  console.log(response[0].statusCode);
}

exports.sendSubmitConfirmationEmail = sendSubmitConfirmationEmail;
exports.sendEditSuccessEmail = sendEditSuccessEmail;
exports.sendResubmitEmail = sendResubmitEmail;
