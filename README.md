# Limitless Firebase Function

Limitless Firebase Functions in this repo is designed to handle the backend logic and integration with external services for the Limitless competition. Major modules of the functions including:

- **anonymous**: generate anonymous sign-in links with encrypted tokens for judging
- **apiServices**: API endpoints for the form application to consume (e.g. paged and filtered user/submission data)
- **auth**: user-related functions including loading and assigning pre-defined user roles
- **backup**: Firestore backup scheduler function
- **cleanupOpenshotExports**: scheduler function for cleaning and restarting dead OpenShot editing jobs
- **dashboard**: paged data for the admin dashboard submission list
- **editComplete**: function triggered when OpenShot edit completes to update the Firestore object status
- **editVideo**: start OpenShot video editing with the editing templates and submission details
- **feedbackEdit**: feedback video editing functions
- **forms**: submission-related API endpoints (e.g get user submissions, post submission, get submission status, etc.)
- **image**: user profile image resizing function
- **judging**: judging-related functions (e.g. create judging allocation, generate judging result spreadsheet)
- **leaderboard**: leaderboard generation scheduler function for the admin dashboard
- **linkedin**: LinkedIn authentication functions for redirection, generating Firebase accounts, and get user profiles from LinkedIn
- **loadData**: import/export static comms/translation data (from and into spreadsheets)
- **pipeline**: functions for starting AWS MediaConvert, AWS Transcribe, Google Speech-to-Text, YouTube video and caption upload services, and webhooks for AWS SNS (for transcoding and transcription job completion/error)
- **restartEdits**: restart submission editing jobs if they fail
- **translation**: generate translation lang objects and machine translations for post-editing tasks on the admin dashboard
- **trigger**: triggering functions called when submission/media/lang object status changes

## External Services

- [AWS MediaConvert](https://aws.amazon.com/it/mediaconvert/)
- [AWS Transcribe](https://aws.amazon.com/transcribe/)
- [AWS Translate](https://aws.amazon.com/translate/)
- [LinkedIn](https://www.linkedin.com/developers/)
- [YouTube Data API V3](https://developers.google.com/youtube/v3/docs)
- [SendGrid](https://sendgrid.com/)
- [Google Translate](https://cloud.google.com/translate): for Persian, use Google Translate for generating machine translations
- [Google Speech-to-Text](https://cloud.google.com/speech-to-text/): AWS Transcribe don't support Bengali, Persian, Punjabi, Urdu, Swahili. Use Google Speech-to-Text as the fallback for generating raw transcriptions for participant submissions
- [OpenShot on AWS](https://cloud.openshot.org/doc/getting_started.html)

## Building, Testing, and Deploying

Running and deploying this project requires the installation of the Firebase CLI. If using Windows, Git Bash doesn't work - Windows Powershell seems to work best.

If you're using Windows Powershell, you will probably have to run the following command each session you want to use the Firebase CLI:

```
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass
```

Please set up the firebase environment variables for LinkedIn and Openshot integration as:

```JSON
{
  "linkedin": {
    "client_secret": "LINKEDIN_CLIENT_SECRET",
    "client_id": "LINKEDIN_CLIENT_ID"
  },
  "openshot": {
    "client_username": "OPENSHOT_CLIENT_USERNAME",
    "client_password": "OPENSHOT_CLIENT_PASSWORD",
    "username": "OPENSHOT_CLIENT_USERNAME",
    "password": "OPENSHOT_CLIENT_PASSWORD",
    "url": "OPENSHOT_URL"
  },
  "anonymous": {
    "encryption_secret": "ENCRYPTION_SECRET",
    "encryption_key": "ENCRYPTION_KEY"
  },
  "openshot_lambda": {
    "secret": "OPENSHOT_LAMBDA_SECRET",
    "endpoint": "LAMBDA_ID.execute-api.REGION.amazonaws.com/default/LAMBDA_NAME"
  },
  "aws": {
    "mediaconvert_endpoint": "https://MEDIACONVERT_ID.mediaconvert.REGION.amazonaws.com",
    "region": "REGION",
    "secret_access_key": "AWS_SECRET_ACCESS_KEY",
    "access_key_id": "AWS_KEY_ID"
  },
  "sendgrid": {
    "sender": "SENDER_EMAIL",
    "api_key": "SENDGRID_API_KEY",
    "early_submission_idea_id": "DYNAMIC_TEMPLATE_ID",
    "early_submission_challenge_id": "DYNAMIC_TEMPLATE_ID",
    "final_submission_id": "DYNAMIC_TEMPLATE_ID",
    "resubmission_id": "DYNAMIC_TEMPLATE_ID",
    "confirmation_id": "DYNAMIC_TEMPLATE_ID"
  },
  "service": {
    "limitless_url": "https://limitless.solferinoacademy.com",
    "admin_url": "https://limitlessadmin.solferinoacademy.com/",
    "function_url": "https://FUNCTION_URL.cloudfunctions.net/"
  },
  "google": {
    "client_id": "GOOGLE_CLIENT_ID.apps.googleusercontent.com",
    "client_secret": "GOOGLE_CLIENT_SECRET",
    "redirect_url": "http://localhost:3000",
    "token_id": "GOOGLE_TOKEN_ID",
    "translation_api_key": "GOOGLE_TRANSLATION_API_KEY"
  }
}
```

Running in the offline emulator will require you to populate the relevant sections in the `.runtimeconfig.json` file with real data. The environment variables should already be set for the live versions. These can be found and stored for local use, using the command:

```
(Mac/Linux) firebase functions:config:get > .runtimeconfig.json
(Windows Powershell) firebase functions:config:get | ac .runtimeconfig.json
```

Once the `.runtimeconfig.json` file is prepared and stored in the `/functions` folder, Functions can be tested locally using the command:

```
firebase emulators:start --only functions
```

You can add new environment variables using the following command:

```
firebase functions:config:set api_name.key_name=value
```

You can then deploy any updated Firebase functions, config variables and rules to the live version with:

```
(All Changes) firebase deploy
(Only Functions and Config Changes) firebase deploy --only functions
(Config Changes and a Specific Function) firebase deploy --only functions:function_name
```

### Importing / Exporting Locale Files

```
./functions/scripts/localeUtil.js import -i locales.xlsx -r test -o ../../../youth-innovation-forms/src/locales

./functions/scripts/localeUtil.js export -i ../../../youth-innovation-forms/src/locales -r test
```

### Create and Export Judging Allocation Spreadsheet

A Firebase HTTPS function has been created for generating the judging allocation spreadsheet with magic links for the judging tasks allocated for each allocation.

Grouping of the allocation is based on common tags for the submission. Associated National Societies will be listed in each allocation so that Solferino Academy can share the magic links to National Societies which don't associate with any judging tasks in that allocation.

The endpoint of the function is:

```
https://us-central1-youthinnovation-firebase.cloudfunctions.net/createJudgingAllocation
```

After successfully calling the function, a spreadsheet will be automatically generated and stored in Firebase Storage under `gs://youthinnovation-firebase.appspot.com/judging/judging_allocation.xlsx` for sending out the judging tasks.
