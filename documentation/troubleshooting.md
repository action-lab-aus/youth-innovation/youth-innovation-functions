# Troubleshooting the pipeline

## Submission stuck on `edited_1`

In this case, it is likely that the openshot server has failed to export an edit.
This can be checked by going to the web frontend for the openshot instance and
looking at the list of "projects" - there will be one or two at the end of the list
who have "exports" in them that failed.

There is a function for automatically cleaning up and restarting failed "exports",
but sometimes this fails too (which should probably be fixed).

In this case, the best course of action is to delete the openshot project(s) with the
bad export, and update the status of the status of the submission in firestore to be
"moderated" - this will retrigger the openshot edit jobs

